package com.jpaz.testspring;

import com.jpaz.testspring.model.User;
import com.jpaz.testspring.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = TestSpringApplication.class)
class TestSpringApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	void whenSelectAllUser_thenAll() {
		Collection<User> users = userRepository.findAll();

		assertNotNull(users);
		assertFalse(users.isEmpty());
	}

}
