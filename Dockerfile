FROM openjdk:13
ADD target/*.jar *.jar
ENV JAVA_OPTS=""
ENV OPTS=""
ENTRYPOINT java $JAVA_OPTS -jar *.jar $OPTS